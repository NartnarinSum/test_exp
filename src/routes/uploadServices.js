const express = require("express");
const router = express.Router();
const multer = require("multer");
const ImageFile = require('../models/ImageFile');
const ImageChunk = require('../models/ImageChunk');
const { GridFsStorage } = require("multer-gridfs-storage");
const GridFSBucket = require("mongodb").GridFSBucket;
const MongoClient = require("mongodb").MongoClient;
const util = require("util");
const {mongoServer,mongoURL} =  require('../config');
const mongoClient = new MongoClient(mongoURL);

////////////////////////

makeCodeGuest = function (number) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < number; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}

let storage = new GridFsStorage({
  url: mongoURL,
  options: { useNewUrlParser: true, useUnifiedTopology: true },
  file: (req, image) => {
    return new Promise(async (resolve, reject) => {
    const match = ["image/png", "image/jpeg"];
    if (match.indexOf(image.mimetype) === -1) {
      const filename = `${Date.now()}-${makeCodeGuest(10)}`;
      resolve(filename);
    }
    resolve( {
      bucketName:mongoServer.BUCKET_FILE_NAME,
      filename: `${Date.now()}-${makeCodeGuest(10)}`
    });
  });
  }
});

let uploadFiles = multer({ storage }).single("image");
//let uploadFilesMiddleware = util.promisify(uploadFiles);

const  uploadFile = (req, res) => {
  return new Promise(async  (resolve, reject) => {
    try {
      uploadFiles(req, res, function (err) {
        if (err){
          console.log(JSON.stringify(err));
          reject({message :'fail saving image',uploadStatus : false});
        } else {

          if (res.req.file){
          let result = {
            message : "upload success",
            uploadStatus : true,
            id :res.req.file.id,
            filename:res.req.file.filename,
            mimetype:res.req.file.mimetype
          }
          resolve(result); 
        }else{
          reject({message :'fail saving image',uploadStatus : false});
        } 

        }
      });
    } catch (error) {
      console.log(error)
      reject(error);
  }
})
}

router.post('/image/', function (req, res) {

  upload(req, res, function (err) {
    if (err){
      console.log(JSON.stringify(err));
      res.status(400).send('fail saving image');
    } else {
      console.log('The filename is ' + res.req.file.filename);
      res.send(res.req.file.filename);  
    }
  });

});


const  getListFiles = async (req, res) => {
  try {

    const cursor = await ImageFile.find({});

    if (cursor.length === 0) {
      return res.status(500).send({
        message: "No files found!",
      });
    }
    let fileInfos = [];
    await cursor.forEach((doc) => {
      fileInfos.push(doc);
    });
    return res.status(200).send(fileInfos);
  } catch (error) {
    return res.status(500).send({
      message: error.message,
    });
  }
};

const download = async (req, res) => {
    try {
      await mongoClient.connect();
      const database = mongoClient.db(mongoServer.DB_NAME);
      const bucket = new GridFSBucket(database, {
        bucketName: mongoServer.BUCKET_FILE_NAME,
      });

      let downloadStream = bucket.openDownloadStreamByName(req.params.name);
      downloadStream.on("data", function (data) {
        return res.status(200).write(data);
      });
      downloadStream.on("error", function (err) {
        return res.status(404).send({ message: "Cannot download the Image!" });
      });
      downloadStream.on("end", () => {
        return res.end();
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
      });
    }
  };



  router.post("/uploadPhoto",async (req, res) => {
    try {
      let result = await uploadFile(req, res);
      res.status(200).send(result);
    } catch (error) {
       res.status(500).send(error);
    }
  });

  router.get("/files",async (req, res) => {
    try {
      await getListFiles(req, res);
    } catch (error) {
       res.send(error);
    }
  });

  router.get("/files/:name",async (req, res) => {
    try {
      await  download(req, res);
    } catch (error) {
       res.send(error);
    }
  });

  //router.get("/files", getListFiles);
  //router.get("/files/:name",  download);



module.exports = {router:router,uploadFile} ;