const express = require("express");
const router = express.Router();
const uploadServices = require('./uploadServices');
const multer = require("multer");
const Book = require('../models/Book');
const ImageFile = require('../models/ImageFile');
const ERROR_ENUM=  require('../service/error');
const {mongoServer,mongoURL} =  require('../config');
const MongoClient = require("mongodb").MongoClient;
const GridFSBucket = require("mongodb").GridFSBucket;
const mongoClient = new MongoClient(mongoURL);

router.get('/getBookAll/', async (req, res) => {
    try {
        let bookDatas = await Book.find().sort({price:1});
        res.send(bookDatas);
      } catch (error) {
        res.status(500).send(error);
      }
});

router.get('/getBook/:id', async (req, res) => {
    try {
        let bookData = await Book.find({_id:req.params.id}).sort({price:1});
        res.send(bookData);
      } catch (error) {
        res.status(500).send(error);
      }
});

router.get('/getBookWithWhere/', async (req, res) => {
    try {
        let bookData = await Book.find(req.query).sort({price:1});
        res.send(bookData);
      } catch (error) {
        res.status(500).send(error);
      }
});
//  Atlas Search index.
router.get('/getBookWithTitle/:title', async (req, res) => {
    try {

        const pipeline = [
            {
                $search: {
                  index: 'title_idx',
                  queryString: {
                    defaultPath: "title",
                    query: req.params.title
                 }
                }
              },
              { $sort: { price: 1} }
        ];

        let bookData = await Book.aggregate(pipeline)
        res.send(bookData);
      } catch (error) {
        res.status(500).send(error);
      }
});

router.post('/postBooks/', async (req, res) => {
    try {
        let bookBody = req.body;
        bookBody.created_at=new Date();
        bookBody.updated_at=new Date();
        let createData = await Book.create(bookBody);
        res.send(createData);
      } catch (error) {
        error.message=ERROR_ENUM.ERROR[error.code]
        res.status(500).send(error);
      }
});


router.post('/postBooksWithImage', async (req, res,next) => {
  try {
    let resultUpload = await uploadServices.uploadFile(req,res);
    let bookBody = req.body;
        bookBody.created_at=new Date();
        bookBody.updated_at=new Date();
        if(resultUpload.id){
          bookBody.image=resultUpload.id
          let createData = await Book.create(bookBody);
          res.send(createData);
        }else {
          res.status(500).send("Check image file or parameter");
        }

    } catch (error) {
      error.message=ERROR_ENUM.ERROR[error.code]
      res.status(500).send(error);
    }
});

router.get('/getImageByBookId/:id', async (req, res,next) => {
  try {
    let bookData = await Book.findOne({_id:req.params.id}).sort({price:1});
    if(bookData?bookData.image && bookData.image !== "":false){
      const cursor = await ImageFile.findOne({_id:bookData.image});
      if(cursor){
      await mongoClient.connect();
      const database = mongoClient.db(mongoServer.DB_NAME);
      const bucket = new GridFSBucket(database, {
        bucketName: mongoServer.BUCKET_FILE_NAME,
      });
      
      let downloadStream = bucket.openDownloadStreamByName(cursor.filename);
      downloadStream.on("data", function (data) {
         res.status(200).write(data);
      });
      downloadStream.on("error", function (err) {
         res.status(404).send({ message: "Cannot download the Image!" });
      });
      downloadStream.on("end", () => {
         res.end();
      });
     }else {
      res.status(404).send({ message: "Can not found image from book" });
     }

    }else{
      res.status(404).send({ message: "Can not found image from book" });
    }
  
  } catch (error) {
     res.status(500).send(error.message);
  }
});



router.patch('/updateBook/:id', async (req, res) => {
    try {
    let updateObject = req.body; 
    let id = req.params.id;

    delete updateObject.created_at
    updateObject.updated_at=new Date();

    let updateResult = await Book.updateOne({_id  : id}, {$set: updateObject});
    if(updateResult.matchedCount){
        let result =  await Book.findOne({_id:req.params.id});
        res.send({message:'Update book id : '+req.params.id+' success.',result}) 
    }else{
        res.status(500).send({message:'Cannot update book id : '+req.params.id+', please check id.'});
    }
      
    
} catch (error) {
    res.status(500).send(error);
  }
});

router.delete('/deleteBook/:id', async (req, res) => {
    try {
        let deleteResult = await Book.deleteOne({_id:req.params.id})
        deleteResult.deletedCount ? res.send({message:'Remove book id : '+req.params.id+' success.'}):res.status(500).send({message:'Cannot remove book id : '+req.params.id+', please check id.'});
      } catch (error) {
        res.status(500).send({message:'Cannot remove book id : '+req.params.id+', please check id.'});
      }
});


module.exports = router ;