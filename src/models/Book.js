const mongoose = require('mongoose'), Schema = mongoose.Schema;

const bookSchema = new Schema({
    //_id : { type: Schema.Types.ObjectId, unique: true },
    title : { type: String, unique: true ,require:true, default: "" },
    description : { type: String, default: ""  },
    image : { type:String, default: "" },
    price : { type: Number, default: 0 },
    created_at : { type: Date  },
    updated_at : {  type: Date  }
});


const Book = mongoose.model('Book', bookSchema, 'Books');
module.exports  = Book;