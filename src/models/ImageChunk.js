const mongoose = require('mongoose'), Schema = mongoose.Schema;

const imageChunkSchema = new Schema({
    //_id : { type: Schema.Types.ObjectId, unique: true },
    files_id : { type: Schema.Types.ObjectId, unique: true },
    n : { type: Number  },
    data : { type: Buffer }
});


const ImageChunk = mongoose.model('ImageChunk', imageChunkSchema, 'image.chunks');
module.exports  = ImageChunk;