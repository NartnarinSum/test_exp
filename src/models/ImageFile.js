const mongoose = require('mongoose'), Schema = mongoose.Schema;

const imageFileSchema = new Schema({
    //_id : { type: Schema.Types.ObjectId, unique: true },
    length : { type: Number },
    chunkSize : { type: Number  },
    uploadDate : { type: Date },
    filename : { type: String },
    contentType : { type: String }
});


const ImageFile = mongoose.model('ImageFile', imageFileSchema, 'image.files');
module.exports  = ImageFile;