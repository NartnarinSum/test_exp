const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require('body-parser');
const {mongoServer,mongoURL} =  require('./src/config');
//const app = require('./src/app');
const port=3000;
// init mongo

mongoose.connect(mongoURL,mongoServer.CONNECT_PARAMS)
.then( () => {
    console.log('Connected to database success ')
})
.catch( (err) => {
    console.error(`Error connecting to the database. \n${err}`);
})

// init express
const app = express();
app.use(express.json()); // Used to parse JSON bodies
app.use(express.urlencoded());
app.set("view engine","ejs");

const getDurationInMilliseconds = (start) => {
    const NS_PER_SEC = 1e9
    const NS_TO_MS = 1e6
    const diff = process.hrtime(start)

    return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS
}

app.use((req, res, next) => {
    const start = process.hrtime()
    res.on('close', () => {
        const durationInMilliseconds = getDurationInMilliseconds (start)
        console.log(`CALL API : ${req.method} ${req.originalUrl} [CLOSED] / TIME DURATION : ${durationInMilliseconds.toLocaleString()} ms`)
    })
    next()
})

// service routes

const bookServices = require("./src/routes/bookServices");
const uploadServices = require("./src/routes/uploadServices");
app.use('/exp_api',bookServices);
app.use('/exp_api',uploadServices.router);


app.listen(port,() => console.log('Start server PORT : '+port+'\n Call API By : http://localhost:'+port+'/exp_api/'));


